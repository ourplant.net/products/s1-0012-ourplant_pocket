Here, you will find an overview of the open source informmation of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s1-0012-ourplant_pocket).


| document | download options |
| -------- | ---------------- |
| operating manual          |[de](https://gitlab.com/ourplant.net/products/s1-0012-ourplant_pocket/-/raw/main/01_operating_manual/S1-0012_A_Betriebsanleitung.pdf)|
|assembly drawing           |[de](https://gitlab.com/ourplant.net/products/s1-0012-ourplant_pocket/-/raw/main/02_assembly_drawing/S1-0012_A_ZNB_Ourplant_Pocket.pdf)|
|circuit diagram            |[de](https://gitlab.com/ourplant.net/products/s1-0012-ourplant_pocket/-/raw/main/03_circuit_diagram/S1-0012-EPLAN-A.pdf)|
|maintenance instructions   ||
|spare parts                ||

