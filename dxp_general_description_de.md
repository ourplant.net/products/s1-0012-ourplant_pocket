Die OurPlant Pocket ist eine Entwicklermaschine für den perfekten Einstieg in die Mikromontage. Mit ihr werden Prozesse bereits im frühesten Konzeptstadium automatisiert abgebildet. Die kompakte Anlage ermöglicht teilautomatisierte Prozesses und bietet Entwicklern somit eine hervorragende Unterstützung beim Testen, Entwickeln und Validieren ihrer Prozesse. Aufgrund der durchgängigen Kompatibilität der OurPlant Plattform profitiert diese Maschine von einer hohen Modulvielfalt. Alle eingerichteten Prozesse können auf alle anderen OurPlant Anlagen übertragen werden.
Nicht zuletzt erfüllt die OurPlant Pocket die Ansprüche aller anderen OurPlant Maschinen in Sachen Bewegungsrichtung, Genauigkeit und Schnittstellen. Aufgrund ihrer geringen Standfläche von 550 mm x 660 mm, ist sie die perfekte Lösung für den Entwicklerarbeitsplatz, aber auch für eine teilautomatisierte Fertigung in der Kleinserienproduktion.

**Vorteile:**
– Entwicklung verschiedenster Teilprozesse auf einer Anlage
– Ideal für Prozess- und Modultests
– Softwarebasierte Systemsteuerung durch einen Laptop
– Geringe Rüstaufwände durch echte Plug&Play-Fähigkeit