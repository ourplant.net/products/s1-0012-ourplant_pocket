The compact OurPlant Pocket realises semi-automated processes and provides developers an excellent support in testing, developing and validating their processes.
Depending on the task, the OurPlant Pocket can be equipped with up to two processing heads and different modules. It benefits from the compatibility of the OurPlant platform and a high variety of modules. All established processes can be transferred to all other OurPlant systems.
Last but not least, the OurPlant Pocket meets the requirements of all other OurPlant machines in terms of movement directions, accuracy and interfaces. Thanks to its small footprint of 550 mm x 660 mm, it is the perfect solution for a developer workstation, but also for semi-automated production in small series.

**Advantages:**
– Development of various subprocesses on one machine
– Ideal for process and module tests
– Software-based system control by laptop
– Shorter set-up times through real plug&play